import Vue from 'vue'
import App from './App.vue'
import routes from './routes';
import VueRouter from 'vue-router'
import axios from 'axios';
import config from './config/config';

Vue.config.productionTip = false;
Vue.use(VueRouter);

// Add a request interceptor
axios.interceptors.request.use(function (config) {
    if (localStorage.getItem('__t')) {
        // Add Token if exists
        config.headers.authorization = localStorage.getItem('__t');
    }
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

// Add a response interceptor
axios.interceptors.response.use(function (response) {
    // Do something with response data
    return response;
}, function (error) {
    // Do something with response error
    return Promise.reject(error);
});

Vue.prototype.$http = axios;
Vue.prototype.$config = config;


// Routes Configuration
const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
})

new Vue({
    render: h => h(App),
    router
}).$mount('#app');
