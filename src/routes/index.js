import Seemless from '../pages/Seemless'
import Bye from '../pages/Bye'

const routes = [
    {path: '/', component: Seemless},
    {path: '/bye', component: Bye}
];


export default routes;